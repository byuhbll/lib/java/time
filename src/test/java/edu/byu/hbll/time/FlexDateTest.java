package edu.byu.hbll.time;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalAmount;
import java.util.Optional;
import org.junit.Test;

/** Unit tests for {@link FlexTime}. */
public class FlexDateTest {

  /**
   * Verifies that calling {@link FlexTime#parse(String)} will resolve to an empty {@link Optional}
   * if a {@code null} string is provided.
   */
  @Test
  public void parseShouldReturnEmptyWhenNull() {
    assertFalse(FlexTime.parse(null).isPresent());
  }

  /**
   * Verifies that calling {@link FlexTime#parse(String)} will resolve to an empty {@link Optional}
   * if an empty string is provided.
   */
  @Test
  public void parseShouldReturnEmptyWhenEmpty() {
    assertFalse(FlexTime.parse("").isPresent());
  }

  /**
   * Verifies that calling {@link FlexTime#parse(String)} will throw a {@link
   * DateTimeParseException} when a non-IS0-8601 string is provided.
   */
  @Test(expected = DateTimeParseException.class)
  public void parseShouldThrowExceptionWhenNonIsoDateIsProvided() {
    FlexTime.parse("20 Mar 2017");
  }

  /**
   * Verifies that calling {@link FlexTime#parse(String, ZoneId)} will resolve to the first instant
   * of a given date.
   */
  @Test
  public void parseShouldReturnFirstInstantOfANonInstantaneousExpression() {
    Instant instant = FlexTime.parse("2015", ZoneId.of("UTC")).orElse(null);

    assertEquals(Instant.parse("2015-01-01T00:00:00Z"), instant);
  }

  /** Verifies that attempting to parse a null value will fail. */
  @Test(expected = NullPointerException.class)
  public void parseBestShouldFailToParseNull() {
    FlexTime.parseBest(null);
  }

  /** Verifies that attempting to parse a non-ISO-8601 string will fail. */
  @Test(expected = DateTimeParseException.class)
  public void parseBestShouldFailToParseBogusString() {
    FlexTime.parseBest("bogus");
  }

  /** Verifies that attempting to parse an ISO-8601 year will return a {@link Year}. */
  @Test
  public void parseBestShouldParseAYear() {
    TemporalAccessor temporalAccessor = FlexTime.parseBest("2015");
    assertEquals(Year.of(2015), temporalAccessor);
  }

  /**
   * Verifies that attempting to parse an ISO-8601 year and month will return an {@link YearMonth}.
   */
  @Test
  public void parseBestShouldParseAYearMonth() {
    TemporalAccessor temporalAccessor = FlexTime.parseBest("2015-02");
    assertEquals(YearMonth.of(2015, 02), temporalAccessor);
  }

  /** Verifies that attempting to parse an ISO-8601 date will return a {@link LocalDate}. */
  @Test
  public void parseBestShouldParseALocalDate() {
    TemporalAccessor temporalAccessor = FlexTime.parseBest("2015-02-01");
    assertEquals(LocalDate.parse("2015-02-01"), temporalAccessor);
  }

  /**
   * Verifies that attempting to parse an ISO-8601 date/time with no timezone data will return a
   * {@link LocalDateTime}.
   */
  @Test
  public void parseBestShouldParseALocalDateTime() {
    TemporalAccessor temporalAccessor = FlexTime.parseBest("2010-11-12T13:14:15");
    assertEquals(LocalDateTime.parse("2010-11-12T13:14:15"), temporalAccessor);
  }

  /**
   * Verifies that attempting to parse an ISO-8601 date/time with zone data will return a {@link
   * ZonedDateTime}.
   */
  @Test
  public void parseBestShouldParseAZonedDateTime() {
    TemporalAccessor temporalAccessor = FlexTime.parseBest("2010-11-12T13:14:15-06:00");
    assertEquals(ZonedDateTime.parse("2010-11-12T13:14:15-06:00"), temporalAccessor);
  }

  /**
   * Verifies that calling {@link FlexTime#parseDuration(String)} with an ISO-8601 Duration that can
   * be parsed as a {@link Duration} object will return the appropriate {@link Duration}.
   */
  @Test
  public void parseDurationShouldParseDurations() {
    TemporalAmount t = FlexTime.parseDuration("P5DT2H5M");

    assertEquals(Duration.parse("P5DT2H5M"), t);
  }

  /**
   * Verifies that calling {@link FlexTime#parseDuration(String)} with an ISO-8601 Duration that
   * cannot be parsed as a {@link Duration} object, but which can be parsed as a {@link Period},
   * will return the appropriate {@link Period}.
   */
  @Test
  public void parseDurationShouldParsePeriods() {
    TemporalAmount t = FlexTime.parseDuration("P1M5D");

    assertEquals(Period.parse("P1M5D"), t);
  }

  /**
   * Verifies that calling {@link FlexTime#parseDuration(String)} with an ISO-8601 Duration that
   * cannot be parsed as a {@link Duration} or {@link Period} will throw an exception.
   */
  @Test(expected = DateTimeParseException.class)
  public void parseDurationShouldFailIfNotDurationOrPeriod() {
    FlexTime.parseDuration("P1M5DT2H5M");
  }

  /**
   * Verifies that calling {@link FlexTime#firstInstant(TemporalAccessor, ZoneId)} will return the
   * first instant in a {@link Year}.
   */
  @Test
  public void firstInstantShouldReturnTheMinInstantInAYear() {
    Year temporal = Year.of(2015);
    Instant instant = FlexTime.firstInstant(temporal, ZoneId.of("UTC"));

    assertEquals(Instant.parse("2015-01-01T00:00:00Z"), instant);
  }

  /**
   * Verifies that calling {@link FlexTime#firstInstant(TemporalAccessor, ZoneId)} will return the
   * first instant in a {@link YearMonth}.
   */
  @Test
  public void firstInstantShouldReturnTheMinInstantInAYearMonth() {
    YearMonth temporal = YearMonth.parse("2016-02");
    Instant instant = FlexTime.firstInstant(temporal, ZoneId.of("UTC"));

    assertEquals(Instant.parse("2016-02-01T00:00:00Z"), instant);
  }

  /**
   * Verifies that calling {@link FlexTime#firstInstant(TemporalAccessor, ZoneId)} will return the
   * first instant in a {@link LocalDate}.
   */
  @Test
  public void firstInstantShouldReturnTheMinInstantInALocalDate() {
    LocalDate temporal = LocalDate.parse("2015-03-01");
    Instant instant = FlexTime.firstInstant(temporal, ZoneId.of("UTC"));

    assertEquals(Instant.parse("2015-03-01T00:00:00Z"), instant);
  }

  /**
   * Verifies that calling {@link FlexTime#firstInstant(TemporalAccessor, ZoneId)} with a {@link
   * LocalDateTime} will return the corresponding {@link Instant} of the given timezone.
   */
  @Test
  public void firstInstantShouldReturnTheInstantOfALocalDateTime() {
    LocalDateTime temporal = LocalDateTime.parse("2010-11-12T13:14:15");
    Instant instant = FlexTime.firstInstant(temporal, ZoneId.of("America/Denver"));

    assertEquals(Instant.parse("2010-11-12T20:14:15Z"), instant);
  }

  /**
   * Verifies that calling {@link FlexTime#firstInstant(TemporalAccessor, ZoneId)} with a {@link
   * OffsetDateTime} will return the corresponding {@link Instant}.
   */
  @Test
  public void firstInstantShouldReturnTheInstantOfAnOffsetDateTime() {
    OffsetDateTime temporal = OffsetDateTime.parse("2010-11-12T13:14:15-07:00");
    Instant instant = FlexTime.firstInstant(temporal, ZoneId.of("UTC"));

    assertEquals(Instant.parse("2010-11-12T20:14:15Z"), instant);
  }

  /**
   * Verifies that calling {@link FlexTime#firstInstant(TemporalAccessor, ZoneId)} with a {@link
   * ZonedDateTime} will return the corresponding {@link Instant}.
   */
  @Test
  public void firstInstantShouldReturnTheInstantOfAZonedDateTime() {
    ZonedDateTime temporal = ZonedDateTime.parse("2010-11-12T13:14:15-07:00");
    Instant instant = FlexTime.firstInstant(temporal, ZoneId.of("UTC"));

    assertEquals(Instant.parse("2010-11-12T20:14:15Z"), instant);
  }

  /**
   * Verifies that calling {@link FlexTime#firstInstant(TemporalAccessor, ZoneId)} with an {@link
   * Instant} will return the same {@link Instant}.
   */
  @Test
  public void firstInstantShouldReturnAnInstant() {
    Instant temporal = Instant.parse("2010-11-12T13:14:15Z");
    Instant instant = FlexTime.firstInstant(temporal, ZoneId.of("UTC"));

    assertEquals(Instant.parse("2010-11-12T13:14:15Z"), instant);
  }

  /**
   * Verifies that calling {@link FlexTime#lastInstant(TemporalAccessor, ZoneId)} will return the
   * last instant in a {@link Year}.
   */
  @Test
  public void lastInstantShouldReturnTheMaxInstantInAYear() {
    Year temporal = Year.of(2015);
    Instant instant = FlexTime.lastInstant(temporal, ZoneId.of("UTC"));

    assertEquals(Instant.parse("2015-12-31T23:59:59.999999999Z"), instant);
  }

  /**
   * Verifies that calling {@link FlexTime#lastInstant(TemporalAccessor, ZoneId)} will return the
   * last instant in a {@link YearMonth}.
   */
  @Test
  public void lastInstantShouldReturnTheMaxInstantInAYearMonth() {
    YearMonth temporal = YearMonth.parse("2016-02");
    Instant instant = FlexTime.lastInstant(temporal, ZoneId.of("UTC"));

    assertEquals(Instant.parse("2016-02-29T23:59:59.999999999Z"), instant);
  }

  /**
   * Verifies that calling {@link FlexTime#lastInstant(TemporalAccessor, ZoneId)} will return the
   * last instant in a {@link LocalDate}.
   */
  @Test
  public void lastInstantShouldReturnTheMaxInstantInALocalDate() {
    LocalDate temporal = LocalDate.parse("2015-03-01");
    Instant instant = FlexTime.lastInstant(temporal, ZoneId.of("UTC"));

    assertEquals(Instant.parse("2015-03-01T23:59:59.999999999Z"), instant);
  }

  /**
   * Verifies that calling {@link FlexTime#firstInstant(TemporalAccessor, ZoneId)} with a {@link
   * LocalDateTime} will return the corresponding {@link Instant} of the given timezone.
   */
  @Test
  public void lastInstantShouldReturnTheInstantOfALocalDateTime() {
    LocalDateTime temporal = LocalDateTime.parse("2010-11-12T13:14:15");
    Instant instant = FlexTime.lastInstant(temporal, ZoneId.of("America/Denver"));

    assertEquals(Instant.parse("2010-11-12T20:14:15Z"), instant);
  }

  /**
   * Verifies that calling {@link FlexTime#lastInstant(TemporalAccessor, ZoneId)} with a {@link
   * OffsetDateTime} will return the corresponding {@link Instant}.
   */
  @Test
  public void lastInstantShouldReturnTheInstantOfAnOffsetDateTime() {
    OffsetDateTime temporal = OffsetDateTime.parse("2010-11-12T13:14:15-07:00");
    Instant instant = FlexTime.firstInstant(temporal, ZoneId.of("UTC"));

    assertEquals(Instant.parse("2010-11-12T20:14:15Z"), instant);
  }

  /**
   * Verifies that calling {@link FlexTime#lastInstant(TemporalAccessor, ZoneId)} with a {@link
   * ZonedDateTime} will return the corresponding {@link Instant}.
   */
  @Test
  public void lastInstantShouldReturnTheInstantOfAZonedDateTime() {
    ZonedDateTime temporal = ZonedDateTime.parse("2010-11-12T13:14:15-07:00");
    Instant instant = FlexTime.lastInstant(temporal, ZoneId.of("UTC"));

    assertEquals(Instant.parse("2010-11-12T20:14:15Z"), instant);
  }

  /**
   * Verifies that calling {@link FlexTime#lastInstant(TemporalAccessor, ZoneId)} with an {@link
   * Instant} will return the same {@link Instant}.
   */
  @Test
  public void lastInstantShouldReturnAnInstant() {
    Instant temporal = Instant.parse("2010-11-12T13:14:15Z");
    Instant instant = FlexTime.firstInstant(temporal, ZoneId.of("UTC"));

    assertEquals(Instant.parse("2010-11-12T13:14:15Z"), instant);
  }
}
