package edu.byu.hbll.time;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import org.junit.Test;

/** Unit tests for {@link ByuHolidays}. */
public class ByuHolidaysTest {

  /**
   * Verifies that calling {@link ByuHolidays#universityHolidays(int)} for a given year repeatedly
   * will result in the same holidays being returned each time in an efficient manner.
   */
  @Test
  public void allHolidays() {
    for (int i = 0; i < 100000; i++) {
      assertEquals(
          Arrays.asList(
              LocalDate.of(2013, Month.JANUARY, 1),
              LocalDate.of(2013, Month.JANUARY, 21),
              LocalDate.of(2013, Month.FEBRUARY, 18),
              LocalDate.of(2013, Month.MAY, 27),
              LocalDate.of(2013, Month.JULY, 4),
              LocalDate.of(2013, Month.JULY, 24),
              LocalDate.of(2013, Month.SEPTEMBER, 2),
              LocalDate.of(2013, Month.NOVEMBER, 28),
              LocalDate.of(2013, Month.NOVEMBER, 29),
              LocalDate.of(2013, Month.DECEMBER, 24),
              LocalDate.of(2013, Month.DECEMBER, 25),
              LocalDate.of(2013, Month.DECEMBER, 31)),
          ByuHolidays.universityHolidays(2013));
    }
  }

  /**
   * Verifies that calling {@link ByuHolidays#beginningOfYear(int)} will return the appropriate
   * dates for a given year.
   */
  @Test
  public void beginningOfYearShouldReturnTheAppropriateDates() {
    assertEquals(
        Arrays.asList(LocalDate.of(2013, Month.JANUARY, 1)), ByuHolidays.beginningOfYear(2013));
    assertEquals(
        Arrays.asList(LocalDate.of(2014, Month.JANUARY, 1)), ByuHolidays.beginningOfYear(2014));
    assertEquals(
        Arrays.asList(LocalDate.of(2015, Month.JANUARY, 1), LocalDate.of(2015, Month.JANUARY, 2)),
        ByuHolidays.beginningOfYear(2015));
    assertEquals(
        Arrays.asList(LocalDate.of(2016, Month.JANUARY, 1)), ByuHolidays.beginningOfYear(2016));
    assertEquals(
        Arrays.asList(LocalDate.of(2017, Month.JANUARY, 2)), ByuHolidays.beginningOfYear(2017));
    assertEquals(
        Arrays.asList(LocalDate.of(2018, Month.JANUARY, 1), LocalDate.of(2018, Month.JANUARY, 2)),
        ByuHolidays.beginningOfYear(2018));
    assertEquals(
        Arrays.asList(LocalDate.of(2019, Month.JANUARY, 1)), ByuHolidays.beginningOfYear(2019));
    assertEquals(
        Arrays.asList(LocalDate.of(2020, Month.JANUARY, 1)), ByuHolidays.beginningOfYear(2020));
  }

  /**
   * Verifies that calling {@link ByuHolidays#martinLutherKingJrDay(int)} will return the
   * appropriate dates for a given year.
   */
  @Test
  public void martinLutherKingJrDayShouldReturnTheAppropriateDates() {
    assertEquals(LocalDate.of(2013, Month.JANUARY, 21), ByuHolidays.martinLutherKingJrDay(2013));
    assertEquals(LocalDate.of(2014, Month.JANUARY, 20), ByuHolidays.martinLutherKingJrDay(2014));
    assertEquals(LocalDate.of(2015, Month.JANUARY, 19), ByuHolidays.martinLutherKingJrDay(2015));
    assertEquals(LocalDate.of(2016, Month.JANUARY, 18), ByuHolidays.martinLutherKingJrDay(2016));
    assertEquals(LocalDate.of(2017, Month.JANUARY, 16), ByuHolidays.martinLutherKingJrDay(2017));
    assertEquals(LocalDate.of(2018, Month.JANUARY, 15), ByuHolidays.martinLutherKingJrDay(2018));
    assertEquals(LocalDate.of(2019, Month.JANUARY, 21), ByuHolidays.martinLutherKingJrDay(2019));
    assertEquals(LocalDate.of(2020, Month.JANUARY, 20), ByuHolidays.martinLutherKingJrDay(2020));
  }

  /**
   * Verifies that calling {@link ByuHolidays#presidentsDay(int)} will return the appropriate dates
   * for a given year.
   */
  @Test
  public void presidentsDayShouldReturnTheAppropriateDates() {
    assertEquals(LocalDate.of(2013, Month.FEBRUARY, 18), ByuHolidays.presidentsDay(2013));
    assertEquals(LocalDate.of(2014, Month.FEBRUARY, 17), ByuHolidays.presidentsDay(2014));
    assertEquals(LocalDate.of(2015, Month.FEBRUARY, 16), ByuHolidays.presidentsDay(2015));
    assertEquals(LocalDate.of(2016, Month.FEBRUARY, 15), ByuHolidays.presidentsDay(2016));
    assertEquals(LocalDate.of(2017, Month.FEBRUARY, 20), ByuHolidays.presidentsDay(2017));
    assertEquals(LocalDate.of(2018, Month.FEBRUARY, 19), ByuHolidays.presidentsDay(2018));
    assertEquals(LocalDate.of(2019, Month.FEBRUARY, 18), ByuHolidays.presidentsDay(2019));
    assertEquals(LocalDate.of(2020, Month.FEBRUARY, 17), ByuHolidays.presidentsDay(2020));
  }

  /**
   * Verifies that calling {@link ByuHolidays#memorialDay(int)} will return the appropriate dates
   * for a given year.
   */
  @Test
  public void memorialDayShouldReturnTheAppropriateDates() {
    assertEquals(LocalDate.of(2013, Month.MAY, 27), ByuHolidays.memorialDay(2013));
    assertEquals(LocalDate.of(2014, Month.MAY, 26), ByuHolidays.memorialDay(2014));
    assertEquals(LocalDate.of(2015, Month.MAY, 25), ByuHolidays.memorialDay(2015));
    assertEquals(LocalDate.of(2016, Month.MAY, 30), ByuHolidays.memorialDay(2016));
    assertEquals(LocalDate.of(2017, Month.MAY, 29), ByuHolidays.memorialDay(2017));
    assertEquals(LocalDate.of(2018, Month.MAY, 28), ByuHolidays.memorialDay(2018));
    assertEquals(LocalDate.of(2019, Month.MAY, 27), ByuHolidays.memorialDay(2019));
    assertEquals(LocalDate.of(2020, Month.MAY, 25), ByuHolidays.memorialDay(2020));
  }

  /**
   * Verifies that calling {@link ByuHolidays#independenceDay(int)} will return the appropriate
   * dates for a given year.
   */
  @Test
  public void independenceDayShouldReturnTheAppropriateDates() {
    assertEquals(LocalDate.of(2013, Month.JULY, 4), ByuHolidays.independenceDay(2013));
    assertEquals(LocalDate.of(2014, Month.JULY, 4), ByuHolidays.independenceDay(2014));
    assertEquals(LocalDate.of(2015, Month.JULY, 3), ByuHolidays.independenceDay(2015));
    assertEquals(LocalDate.of(2016, Month.JULY, 4), ByuHolidays.independenceDay(2016));
    assertEquals(LocalDate.of(2017, Month.JULY, 4), ByuHolidays.independenceDay(2017));
    assertEquals(LocalDate.of(2018, Month.JULY, 4), ByuHolidays.independenceDay(2018));
    assertEquals(LocalDate.of(2019, Month.JULY, 4), ByuHolidays.independenceDay(2019));
    assertEquals(LocalDate.of(2020, Month.JULY, 3), ByuHolidays.independenceDay(2020));
  }

  /**
   * Verifies that calling {@link ByuHolidays#pioneerDay(int)} will return the appropriate dates for
   * a given year.
   */
  @Test
  public void pioneerDayShouldReturnTheAppropriateDates() {
    assertEquals(LocalDate.of(2013, Month.JULY, 24), ByuHolidays.pioneerDay(2013));
    assertEquals(LocalDate.of(2014, Month.JULY, 24), ByuHolidays.pioneerDay(2014));
    assertEquals(LocalDate.of(2015, Month.JULY, 24), ByuHolidays.pioneerDay(2015));
    assertEquals(LocalDate.of(2016, Month.JULY, 25), ByuHolidays.pioneerDay(2016));
    assertEquals(LocalDate.of(2017, Month.JULY, 24), ByuHolidays.pioneerDay(2017));
    assertEquals(LocalDate.of(2018, Month.JULY, 24), ByuHolidays.pioneerDay(2018));
    assertEquals(LocalDate.of(2019, Month.JULY, 24), ByuHolidays.pioneerDay(2019));
    assertEquals(LocalDate.of(2020, Month.JULY, 24), ByuHolidays.pioneerDay(2020));
  }

  /**
   * Verifies that calling {@link ByuHolidays#laborDay(int)} will return the appropriate dates for a
   * given year.
   */
  @Test
  public void laborDayShouldReturnTheAppropriateDates() {
    assertEquals(LocalDate.of(2013, Month.SEPTEMBER, 2), ByuHolidays.laborDay(2013));
    assertEquals(LocalDate.of(2014, Month.SEPTEMBER, 1), ByuHolidays.laborDay(2014));
    assertEquals(LocalDate.of(2015, Month.SEPTEMBER, 7), ByuHolidays.laborDay(2015));
    assertEquals(LocalDate.of(2016, Month.SEPTEMBER, 5), ByuHolidays.laborDay(2016));
    assertEquals(LocalDate.of(2017, Month.SEPTEMBER, 4), ByuHolidays.laborDay(2017));
    assertEquals(LocalDate.of(2018, Month.SEPTEMBER, 3), ByuHolidays.laborDay(2018));
    assertEquals(LocalDate.of(2019, Month.SEPTEMBER, 2), ByuHolidays.laborDay(2019));
    assertEquals(LocalDate.of(2020, Month.SEPTEMBER, 7), ByuHolidays.laborDay(2020));
  }

  /**
   * Verifies that calling {@link ByuHolidays#thanksgiving(int)} will return the appropriate dates
   * for a given year.
   */
  @Test
  public void thanksgivingShouldReturnTheAppropriateDates() {
    assertEquals(
        Arrays.asList(
            LocalDate.of(2013, Month.NOVEMBER, 28), LocalDate.of(2013, Month.NOVEMBER, 29)),
        ByuHolidays.thanksgiving(2013));
    assertEquals(
        Arrays.asList(
            LocalDate.of(2014, Month.NOVEMBER, 27), LocalDate.of(2014, Month.NOVEMBER, 28)),
        ByuHolidays.thanksgiving(2014));
    assertEquals(
        Arrays.asList(
            LocalDate.of(2015, Month.NOVEMBER, 26), LocalDate.of(2015, Month.NOVEMBER, 27)),
        ByuHolidays.thanksgiving(2015));
    assertEquals(
        Arrays.asList(
            LocalDate.of(2016, Month.NOVEMBER, 24), LocalDate.of(2016, Month.NOVEMBER, 25)),
        ByuHolidays.thanksgiving(2016));
    assertEquals(
        Arrays.asList(
            LocalDate.of(2017, Month.NOVEMBER, 23), LocalDate.of(2017, Month.NOVEMBER, 24)),
        ByuHolidays.thanksgiving(2017));
    assertEquals(
        Arrays.asList(
            LocalDate.of(2018, Month.NOVEMBER, 22), LocalDate.of(2018, Month.NOVEMBER, 23)),
        ByuHolidays.thanksgiving(2018));
    assertEquals(
        Arrays.asList(
            LocalDate.of(2019, Month.NOVEMBER, 28), LocalDate.of(2019, Month.NOVEMBER, 29)),
        ByuHolidays.thanksgiving(2019));
    assertEquals(
        Arrays.asList(
            LocalDate.of(2020, Month.NOVEMBER, 26), LocalDate.of(2020, Month.NOVEMBER, 27)),
        ByuHolidays.thanksgiving(2020));
  }

  /**
   * Verifies that calling {@link ByuHolidays#christmas(int)} will return the appropriate dates for
   * a given year.
   */
  @Test
  public void christmasShouldReturnTheAppropriateDates() {
    assertEquals(
        Arrays.asList(
            LocalDate.of(2013, Month.DECEMBER, 24), LocalDate.of(2013, Month.DECEMBER, 25)),
        ByuHolidays.christmas(2013));
    assertEquals(
        Arrays.asList(
            LocalDate.of(2014, Month.DECEMBER, 25), LocalDate.of(2014, Month.DECEMBER, 26)),
        ByuHolidays.christmas(2014));
    assertEquals(
        Arrays.asList(
            LocalDate.of(2015, Month.DECEMBER, 24), LocalDate.of(2015, Month.DECEMBER, 25)),
        ByuHolidays.christmas(2015));
    assertEquals(
        Arrays.asList(
            LocalDate.of(2016, Month.DECEMBER, 23), LocalDate.of(2016, Month.DECEMBER, 26)),
        ByuHolidays.christmas(2016));
    assertEquals(
        Arrays.asList(
            LocalDate.of(2017, Month.DECEMBER, 25), LocalDate.of(2017, Month.DECEMBER, 26)),
        ByuHolidays.christmas(2017));
    assertEquals(
        Arrays.asList(
            LocalDate.of(2018, Month.DECEMBER, 24), LocalDate.of(2018, Month.DECEMBER, 25)),
        ByuHolidays.christmas(2018));
    assertEquals(
        Arrays.asList(
            LocalDate.of(2019, Month.DECEMBER, 24), LocalDate.of(2019, Month.DECEMBER, 25)),
        ByuHolidays.christmas(2019));
    assertEquals(
        Arrays.asList(
            LocalDate.of(2020, Month.DECEMBER, 24), LocalDate.of(2020, Month.DECEMBER, 25)),
        ByuHolidays.christmas(2020));
  }

  /**
   * Verifies that calling {@link ByuHolidays#endOfYear(int)} will return the appropriate dates for
   * a given year.
   */
  @Test
  public void endOfYearShouldReturnTheAppropriateDates() {
    assertEquals(
        Arrays.asList(LocalDate.of(2013, Month.DECEMBER, 31)), ByuHolidays.endOfYear(2013));
    assertEquals(Arrays.asList(), ByuHolidays.endOfYear(2014));
    assertEquals(
        Arrays.asList(LocalDate.of(2015, Month.DECEMBER, 31)), ByuHolidays.endOfYear(2015));
    assertEquals(
        Arrays.asList(LocalDate.of(2016, Month.DECEMBER, 30)), ByuHolidays.endOfYear(2016));
    assertEquals(Arrays.asList(), ByuHolidays.endOfYear(2017));
    assertEquals(
        Arrays.asList(LocalDate.of(2018, Month.DECEMBER, 31)), ByuHolidays.endOfYear(2018));
    assertEquals(
        Arrays.asList(LocalDate.of(2019, Month.DECEMBER, 31)), ByuHolidays.endOfYear(2019));
    assertEquals(
        Arrays.asList(LocalDate.of(2020, Month.DECEMBER, 31)), ByuHolidays.endOfYear(2020));
  }

  /** Verifies that {@link ByuHolidays#isWorkingDay(LocalDate)} will return false on Saturdays. */
  @Test
  public void isWorkingDayShouldReturnFalseOnSaturdays() {
    assertFalse(ByuHolidays.isWorkingDay(LocalDate.parse("2015-10-10")));
    assertFalse(ByuHolidays.isWorkingDay(LocalDate.parse("2016-10-15")));
    assertFalse(ByuHolidays.isWorkingDay(LocalDate.parse("2017-10-14")));
  }

  /** Verifies that {@link ByuHolidays#isWorkingDay(LocalDate)} will return false on Sundays. */
  @Test
  public void isWorkingDayShouldReturnFalseOnSundays() {
    assertFalse(ByuHolidays.isWorkingDay(LocalDate.parse("2015-10-11")));
    assertFalse(ByuHolidays.isWorkingDay(LocalDate.parse("2016-10-16")));
    assertFalse(ByuHolidays.isWorkingDay(LocalDate.parse("2017-10-15")));
  }

  /**
   * Verifies that {@link ByuHolidays#isWorkingDay(LocalDate)} will return true on normal weekdays.
   */
  @Test
  public void isWorkingDayShouldReturnTrueOnNormalWeekdays() {
    assertTrue(ByuHolidays.isWorkingDay(LocalDate.parse("2015-10-12")));
    assertTrue(ByuHolidays.isWorkingDay(LocalDate.parse("2015-10-13")));
    assertTrue(ByuHolidays.isWorkingDay(LocalDate.parse("2015-10-14")));
    assertTrue(ByuHolidays.isWorkingDay(LocalDate.parse("2015-10-15")));
    assertTrue(ByuHolidays.isWorkingDay(LocalDate.parse("2015-10-16")));
  }

  /**
   * Verifies that {@link ByuHolidays#isWorkingDay(LocalDate)} will return false on university
   * holidays.
   */
  @Test
  public void isWorkingDayShouldReturnFalseOnUniversityHolidays() {
    for (LocalDate holiday : ByuHolidays.universityHolidays(2015)) {
      assertFalse(ByuHolidays.isWorkingDay(holiday));
    }
  }
}
