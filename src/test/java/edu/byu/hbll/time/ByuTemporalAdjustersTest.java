package edu.byu.hbll.time;

import static org.junit.Assert.assertEquals;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZonedDateTime;
import org.junit.Test;

/** Unit tests for {@link ByuTemporalAdjusters}. */
public class ByuTemporalAdjustersTest {

  /**
   * Verifies that {@link ByuTemporalAdjusters#plusWorkingDays(int)} will not skip non-holiday
   * weekdays.
   *
   * <p>Since 2015-01-05 is a Monday in a week without holidays, adding 4 working days will return
   * 2016-01-09, the same as adding 4 chronological days.
   */
  @Test
  public void addByuWorkingDays_shouldCorrectlyAddJustWorkingDays() {
    LocalDate date =
        LocalDate.of(2015, Month.JANUARY, 5).with(ByuTemporalAdjusters.plusWorkingDays(4));

    assertEquals(LocalDate.of(2015, Month.JANUARY, 9), date);
  }

  /**
   * Verifies that {@link ByuTemporalAdjusters#plusWorkingDays(int)} will skip weekends (Saturdays
   * and Sundays).
   *
   * <p>Since 2015-01-10 and 2015-01-11 are both weekend days, adding 4 working days to 2015-01-06
   * will return 2015-01-12 instead of 2015-01-10 (the result of adding 4 chronological days).
   */
  @Test
  public void addByuWorkingDays_shouldSkipWeekends() {
    LocalDate date =
        LocalDate.of(2015, Month.JANUARY, 6).with(ByuTemporalAdjusters.plusWorkingDays(4));

    assertEquals(LocalDate.of(2015, Month.JANUARY, 12), date);
  }

  /**
   * Verifies that {@link ByuTemporalAdjusters#plusWorkingDays(int)} will skip a university holiday.
   *
   * <p>Normally, adding 2 working days to a Tuesday would return the following Thursday. But since
   * 2014-07-24 is Pioneer Day, a BYU University Holiday, adding 2 working days to 2014-07-22 will
   * return Friday, 2014-07-25, instead of Thursday, 2014-07-24.
   */
  @Test
  public void addByuWorkingDays_shouldSkipHolidays() {
    LocalDate date =
        LocalDate.of(2014, Month.JULY, 22).with(ByuTemporalAdjusters.plusWorkingDays(2));
    assertEquals(LocalDate.of(2014, Month.JULY, 25), date);
  }

  /**
   * Verifies that {@link ByuTemporalAdjusters#plusWorkingDays(int)} will skip both holidays and
   * weekends occurring together.
   *
   * <p>2015-01-15 is a Thursday prior to a three-day weekend (due to Martin Luther King Jr. Day, a
   * BYU University Holiday). As a result, adding 4 business days will return Thursday, 2015-01-22,
   * instead of Sunday, 2015-01-19.
   */
  @Test
  public void plusWorkingDaysShouldSkipHolidaysAndWeekends() {
    LocalDate date =
        LocalDate.of(2015, Month.JANUARY, 15).with(ByuTemporalAdjusters.plusWorkingDays(4));
    assertEquals(LocalDate.of(2015, Month.JANUARY, 22), date);
  }

  /**
   * Verifies that providing an {@link Instant} to {@link ByuTemporalAdjusters#plusWorkingDays(int)}
   * will work.
   *
   * <p>Instants are commonly used in applications, but are not calendar-based Temporal objects, so
   * they are given special attention to make sure that they will still adjust as expected.
   */
  @Test
  public void plusWorkingDaysShouldAdjustInstant() {
    Instant instant =
        Instant.parse("2015-01-15T07:00:00Z").with(ByuTemporalAdjusters.plusWorkingDays(4));
    assertEquals(Instant.parse("2015-01-22T07:00:00Z"), instant);
  }

  /**
   * Verifies that {@link ByuTemporalAdjusters#plusWorkingDays(int)} will actually subtract days
   * instead of adding them if a negative integer is provided as the argument.
   *
   * <p>Support for negative numbers is guaranteed so that the adjuster matches the existing
   * behavior offered by the {@link LocalDate#plusDays(long)} method.
   */
  @Test
  public void plusWorkingDaysShouldSubtractDaysIfANegativeNumberIsProvided() {
    LocalDate date =
        LocalDate.of(2020, Month.JANUARY, 21).with(ByuTemporalAdjusters.plusWorkingDays(-4));
    assertEquals(LocalDate.of(2020, Month.JANUARY, 14), date);
  }

  /**
   * Verifies that {@link ByuTemporalAdjusters#minusWorkingDays(int)} will not skip non-holiday
   * weekdays.
   *
   * <p>Since 2015-01-09 is a Friday in a week without holidays, subtracting 4 working days will
   * return 2016-01-04, the same as subtracting 4 chronological days.
   */
  @Test
  public void minusWorkingDaysShouldCorrectlySubtractJustWorkingDays() {
    LocalDate date =
        LocalDate.of(2015, Month.JANUARY, 9).with(ByuTemporalAdjusters.minusWorkingDays(4));
    assertEquals(LocalDate.of(2015, Month.JANUARY, 5), date);
  }

  /**
   * Verifies that {@link ByuTemporalAdjusters#minusWorkingDays(int)} will skip weekends (Saturdays
   * and Sundays).
   *
   * <p>Since 2015-01-10 and 2015-01-11 are both weekend days, subtracting 4 working days from
   * Monday, 2015-01-12, will return 2015-01-06 instead of 2015-01-08 (the result of subtracting 4
   * chronological days).
   */
  @Test
  public void minusWorkingDaysShouldSkipWeekends() {
    LocalDate date =
        LocalDate.of(2015, Month.JANUARY, 12).with(ByuTemporalAdjusters.minusWorkingDays(4));
    assertEquals(LocalDate.of(2015, Month.JANUARY, 6), date);
  }

  /**
   * Verifies that {@link ByuTemporalAdjusters#minusWorkingDays(int)} will skip a university
   * holiday.
   *
   * <p>Normally, subtracting 2 working days from a Friday would return the previous Wednesday. But
   * since 2014-07-24 is Pioneer Day, a BYU University Holiday, subtracting 4 working days from
   * 2014-07-25 will return Tuesday, 2015-07-22, instead of Wednesday, 2014-07-23.
   */
  @Test
  public void minusWorkingDaysShouldSkipHolidays() {
    LocalDate date =
        LocalDate.of(2014, Month.JULY, 25).with(ByuTemporalAdjusters.minusWorkingDays(2));
    assertEquals(LocalDate.of(2014, Month.JULY, 22), date);
  }

  /**
   * Verifies that {@link ByuTemporalAdjusters#minusWorkingDays(int)} will skip both holidays and
   * weekends occurring together.
   *
   * <p>2015-01-21 is a Tuesday following a three-day weekend (due to Martin Luther King Jr. Day, a
   * BYU University Holiday). As a result, subtracting 4 business days will return Tuesday,
   * 2015-01-14, instead of Friday, 2015-01-17.
   */
  @Test
  public void minusWorkingDaysShouldSkipHolidaysAndWeekends() {
    LocalDate date =
        LocalDate.of(2015, Month.JANUARY, 21).with(ByuTemporalAdjusters.minusWorkingDays(4));
    assertEquals(LocalDate.of(2015, Month.JANUARY, 14), date);
  }

  /**
   * Verifies that providing an {@link Instant} to {@link
   * ByuTemporalAdjusters#minusWorkingDays(int)} will work.
   *
   * <p>Instants are commonly used in applications, but are not calendar-based Temporal objects, so
   * they are given special attention to make sure that they will still adjust as expected.
   */
  @Test
  public void minusWorkingDaysShouldAdjustInstant() {
    Instant instant =
        Instant.parse("2015-01-15T07:00:00Z").with(ByuTemporalAdjusters.minusWorkingDays(4));
    assertEquals(Instant.parse("2015-01-09T07:00:00Z"), instant);
  }

  /**
   * Verifies that {@link ByuTemporalAdjusters#minusWorkingDays(int)} will actually add days instead
   * of subtracting them if a negative integer is provided as the argument.
   *
   * <p>Support for negative numbers is guaranteed so that the adjuster matches the existing
   * behavior offered by the {@link LocalDate#minusDays(long)} method.
   */
  @Test
  public void minusWorkingDaysShouldAddDaysIfANegativeNumberIsProvided() {
    LocalDate date =
        LocalDate.of(2020, Month.JANUARY, 17).with(ByuTemporalAdjusters.minusWorkingDays(-4));
    assertEquals(LocalDate.of(2020, Month.JANUARY, 24), date);
  }

  /**
   * This adjuster should return a time arbitrarily long into the future. For an Instant, this is
   * met cleanly by adjusting to Instant.MAX.
   */
  @Test
  public void never_shouldAdjustInstant() {
    Instant instant = Instant.parse("2015-01-15T07:00:00Z").with(ByuTemporalAdjusters.never());

    assertEquals(Instant.MAX, instant);
  }

  /**
   * This adjuster should return a time arbitrarily long into the future. For an calendar-based
   * temporal object, this is met by adjusting to LocalDate.MAX.
   */
  @Test
  public void never_shouldAdjustLocalDate() {
    LocalDate date = LocalDate.of(2015, Month.JANUARY, 15).with(ByuTemporalAdjusters.never());

    assertEquals(LocalDate.MAX, date);
  }

  /**
   * This adjuster should return a time arbitrarily long into the future. For an calendar-based
   * temporal object, this is met by adjusting to LocalDate.MAX.
   */
  @Test
  public void never_shouldAdjustZonedDateTime() {
    ZonedDateTime dateTime =
        ZonedDateTime.parse("2015-01-15T07:00:00Z").with(ByuTemporalAdjusters.never());

    assertEquals(LocalDate.MAX, dateTime.toLocalDate());
  }
}
