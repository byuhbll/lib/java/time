package edu.byu.hbll.time;

import java.time.DateTimeException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.format.SignStyle;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalAmount;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

/**
 * Provides utility methods for easily parsing a variety of ISO date/time formats.
 *
 * <p>All methods in this class are thread-safe, "pure" functions.
 */
public class FlexTime {

  /**
   * A {@link DateTimeFormatter} which can parse the following ISO-based {@link TemporalAccessor}s:
   * {@link Year}, {@link YearMonth}, {@link LocalDate}, {@link LocalDateTime}, {@link
   * ZonedDateTime}.
   */
  public static final DateTimeFormatter FORMATTER =
      new DateTimeFormatterBuilder()
          .parseCaseInsensitive()
          .appendValue(ChronoField.YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
          .optionalStart()
          .appendLiteral('-')
          .appendValue(ChronoField.MONTH_OF_YEAR, 2)
          .optionalStart()
          .appendLiteral('-')
          .appendValue(ChronoField.DAY_OF_MONTH, 2)
          .optionalStart()
          .appendLiteral('T')
          .append(DateTimeFormatter.ISO_TIME)
          .toFormatter();

  /**
   * Returns an optional {@link Instant} corresponding to the provided text, or an empty {@link
   * Optional} if the text is {@code null} or empty.
   *
   * <p>This method is a merely a convenience wrapper for calling {@code parse(text,
   * FlexTime::firstInstant)}.
   *
   * @param text the text to be parsed; this text should be blank ({@code null} or empty) or
   *     correspond with an ISO-8601 year, year-month, date, or date/time.\
   * @return the resulting optional {@link Instant}, or an empty {@link Optional} if the provided
   *     text was blank ({@code null} or empty)
   * @throws DateTimeParseException if the provided text was not blank but could not be parsed as an
   *     ISO-8601 date/time
   */
  public static Optional<Instant> parse(String text) throws DateTimeParseException {
    return parse(text, FlexTime::firstInstant);
  }

  /**
   * Returns an optional {@link Instant} corresponding to the provided text, or an empty {@link
   * Optional} if the text is {@code null} or empty.
   *
   * <p>This method is a merely a convenience wrapper for calling {@code parse(text,
   * temporalAccessor -> FlexTimefirstInstant(temporalAccessor, defaultZoneId)}.
   *
   * @param text the text to be parsed; this text should be blank ({@code null} or empty) or
   *     correspond with an ISO-8601 year, year-month, date, or date/time.\
   * @param defaultZoneId the timezone to use to resolve local dates and times
   * @return the resulting optional {@link Instant}, or an empty {@link Optional} if the provided
   *     text was blank ({@code null} or empty)
   * @throws DateTimeParseException if the provided text was not blank but could not be parsed as an
   *     ISO-8601 date/time
   */
  public static Optional<Instant> parse(String text, ZoneId defaultZoneId)
      throws DateTimeParseException {
    return parse(text, temporalAccessor -> FlexTime.firstInstant(temporalAccessor, defaultZoneId));
  }

  /**
   * Returns an optional {@link Instant} corresponding to the provided text, or an empty {@link
   * Optional} if the text is {@code null} or empty.
   *
   * @param text the text to be parsed; this text should be blank ({@code null} or empty) or
   *     correspond with an ISO-8601 year, year-month, date, or date/time.\
   * @param disambiguationFunction the function to use to map a non-instantaneous expression (such
   *     as a year or date) to a discrete {@link Instant} of time; for most situations, it is
   *     recommended that the {@link #firstInstant(TemporalAccessor)} or {@link
   *     #lastInstant(TemporalAccessor)} functions be supplied
   * @return the resulting optional {@link Instant}, or an empty {@link Optional} if the provided
   *     text was blank ({@code null} or empty)
   * @throws DateTimeParseException if the provided text was not blank but could not be parsed as an
   *     ISO-8601 date/time
   */
  public static Optional<Instant> parse(
      String text, Function<TemporalAccessor, Instant> disambiguationFunction)
      throws DateTimeParseException {
    Objects.requireNonNull(disambiguationFunction, "disambiguationFunction cannot be null");

    return Optional.ofNullable(text)
        .filter(str -> !str.isEmpty())
        .map(FlexTime::parseBest)
        .map(disambiguationFunction);
  }

  /**
   * Uses the {@link #FORMATTER} {@link DateTimeFormatter} included with this class to attempt to
   * parse the given text as a TemporalAccessor.
   *
   * <p>The returned {@link TemporalAccessor} object will be one of the following types, in order of
   * preference:
   *
   * <ul>
   *   <li>{@link ZonedDateTime}
   *   <li>{@link OffsetDateTime}
   *   <li>{@link LocalDateTime}
   *   <li>{@link LocalDate}
   *   <li>{@link YearMonth}
   *   <li>{@link Year}
   * </ul>
   *
   * @param text an ISO-8601 compliant string to parse
   * @return the parsed temporal object
   * @throws DateTimeParseException if the given text cannot be parsed as one of the above {@link
   *     TemporalAccessor}s
   */
  public static TemporalAccessor parseBest(String text) throws DateTimeParseException {
    return FORMATTER.parseBest(
        text,
        ZonedDateTime::from,
        OffsetDateTime::from,
        LocalDateTime::from,
        LocalDate::from,
        YearMonth::from,
        Year::from);
  }

  /**
   * Parses the given text as a {@link TemporalAmount}. If the text can be parsed as a {@link
   * Duration}, then that type will be returned. Otherwise, {@link Period} will be used as a
   * fallback.
   *
   * @param text the text to be parsed
   * @return the resulting {@link Duration} or {@link Period} as described
   * @throws DateTimeParseException if the text cannot be parsed as either a {@link Duration} or
   *     {@link Period}
   */
  public static TemporalAmount parseDuration(String text) throws DateTimeParseException {
    TemporalAmount amount;
    try {
      amount = Duration.parse(text);
    } catch (DateTimeParseException e) {
      amount = Period.parse(text);
    }
    return amount;
  }

  /**
   * Converts the given {@link TemporalAccessor} to an {@link Instant}, using the system default
   * {@link ZoneId} and padding down if necessary to the beginning of the given temporal period.
   *
   * <p>If the provided {@link TemporalAccessor} represents a discrete instant on the timeline
   * ({@link Instant}, {@link ZonedDateTime}, {@link OffsetDateTime}, or {@link LocalDateTime}), it
   * will be converted to an {@link Instant} and returned. In the case of {@link LocalDateTime},
   * {@link ZoneId#systemDefault()} will be used to supply the missing timezone information prior to
   * converting to an {@link Instant}.
   *
   * <p>If the provided {@link TemporalAccessor} does not represent a discrete instant on the
   * timeline but rather a period of time ({@link LocalDate}, {@link YearMonth}, or {@link Year}),
   * the first instant on the timeline in belonging to that period of time (in the system default
   * timezone) will be returned instead.
   *
   * @param temporalAccessor the temporal accessor to be converted to an {@link Instant}
   * @return the resulting instant as defined above
   */
  public static Instant firstInstant(TemporalAccessor temporalAccessor) {
    return firstInstant(temporalAccessor, ZoneId.systemDefault());
  }

  /**
   * Converts the given {@link TemporalAccessor} to an {@link Instant}, using the given {@link
   * ZoneId} and padding down if necessary to the beginning of the given temporal period.
   *
   * <p>If the provided {@link TemporalAccessor} represents a discrete instant on the timeline
   * ({@link Instant}, {@link ZonedDateTime}, {@link OffsetDateTime}, or {@link LocalDateTime}), it
   * will be converted to an {@link Instant} and returned. In the case of {@link LocalDateTime}, the
   * given {@link ZoneId} will be used to supply the missing timezone information prior to
   * converting to an {@link Instant}.
   *
   * <p>If the provided {@link TemporalAccessor} does not represent a discrete instant on the
   * timeline but rather a period of time ({@link LocalDate}, {@link YearMonth}, or {@link Year}),
   * the first instant on the timeline in belonging to that period of time (in the provided
   * timezone) will be returned instead.
   *
   * @param temporalAccessor the temporal accessor to be converted to an {@link Instant}
   * @param defaultZoneId the timezone to use to resolve local dates and times
   * @return the resulting instant as defined above
   */
  public static Instant firstInstant(TemporalAccessor temporalAccessor, ZoneId defaultZoneId) {
    Objects.requireNonNull(temporalAccessor);
    Objects.requireNonNull(defaultZoneId);

    // If the temporal accessor has the resolution of a LocalDate or less, convert it to the
    // appropriate LocalDate (advanced by one day) and move to the next step.
    if (temporalAccessor instanceof Year) {
      temporalAccessor = ((Year) temporalAccessor).atDay(1);
    } else if (temporalAccessor instanceof YearMonth) {
      temporalAccessor = ((YearMonth) temporalAccessor).atDay(1);
    }

    // If the temporal accessor has the resolution of a LocalDate or less, it will now be a
    // LocalDate and will have been advanced by one day in the previous step.  Get the date/time at
    // the start of the day, then subtract one nanosecond from the LocalDateTime to get the last
    // possible date/time of the previous day.
    if (temporalAccessor instanceof LocalDate) {
      temporalAccessor = ((LocalDate) temporalAccessor).atStartOfDay();
    }

    return toInstant(temporalAccessor, defaultZoneId);
  }

  /**
   * Converts the given {@link TemporalAccessor} to an {@link Instant}, using the system default
   * {@link ZoneId} and padding up if necessary to the beginning of the given temporal period.
   *
   * <p>If the provided {@link TemporalAccessor} represents a discrete instant on the timeline
   * ({@link Instant}, {@link ZonedDateTime}, {@link OffsetDateTime}, or {@link LocalDateTime}), it
   * will be converted to an {@link Instant} and returned. In the case of {@link LocalDateTime},
   * {@link ZoneId#systemDefault()} will be used to supply the missing timezone information prior to
   * converting to an {@link Instant}.
   *
   * <p>If the provided {@link TemporalAccessor} does not represent a discrete instant on the
   * timeline but rather a period of time ({@link LocalDate}, {@link YearMonth}, or {@link Year}),
   * the last instant on the timeline in belonging to that period of time (in the system default
   * timezone) will be returned instead.
   *
   * @param temporalAccessor the temporal accessor to be converted to an {@link Instant}
   * @return the resulting instant as defined above
   */
  public static Instant lastInstant(TemporalAccessor temporalAccessor) {
    return lastInstant(temporalAccessor, ZoneId.systemDefault());
  }

  /**
   * Converts the given {@link TemporalAccessor} to an {@link Instant}, using the given {@link
   * ZoneId} and padding up if necessary to the beginning of the given temporal period.
   *
   * <p>If the provided {@link TemporalAccessor} represents a discrete instant on the timeline
   * ({@link Instant}, {@link ZonedDateTime}, {@link OffsetDateTime}, or {@link LocalDateTime}), it
   * will be converted to an {@link Instant} and returned. In the case of {@link LocalDateTime}, the
   * given {@link ZoneId} will be used to supply the missing timezone information prior to
   * converting to an {@link Instant}.
   *
   * <p>If the provided {@link TemporalAccessor} does not represent a discrete instant on the
   * timeline but rather a period of time ({@link LocalDate}, {@link YearMonth}, or {@link Year}),
   * the last instant on the timeline in belonging to that period of time (in the provided timezone)
   * will be returned instead.
   *
   * @param temporalAccessor the temporal accessor to be converted to an {@link Instant}
   * @param defaultZoneId the timezone to use to resolve local dates and times
   * @return the resulting instant as defined above
   */
  public static Instant lastInstant(TemporalAccessor temporalAccessor, ZoneId defaultZoneId) {
    Objects.requireNonNull(temporalAccessor);

    // If the temporal accessor has the resolution of a LocalDate or less, convert it to the
    // appropriate LocalDate (advanced by one day) and move to the next step.
    if (temporalAccessor instanceof LocalDate) {
      temporalAccessor = ((LocalDate) temporalAccessor).plusDays(1L);
    } else if (temporalAccessor instanceof YearMonth) {
      temporalAccessor = ((YearMonth) temporalAccessor).atDay(1).plusMonths(1L);
    } else if (temporalAccessor instanceof Year) {
      temporalAccessor = ((Year) temporalAccessor).atDay(1).plusYears(1L);
    }

    // If the temporal accessor has the resolution of a LocalDate or less, it will now be a
    // LocalDate and will have been advanced by one day in the previous step.  Get the date/time at
    // the start of the day, then subtract one nanosecond from the LocalDateTime to get the last
    // possible date/time of the previous day.
    if (temporalAccessor instanceof LocalDate) {
      temporalAccessor = ((LocalDate) temporalAccessor).atStartOfDay().minusNanos(1L);
    }

    return toInstant(temporalAccessor, defaultZoneId);
  }

  /**
   * Converts an instantaneous point of time to an {@link Instant}.
   *
   * @param temporalAccessor the temporal accessor to be converted to an {@link Instant}
   * @param defaultZoneId the timezone to use to resolve local dates and times
   * @return the resulting instant as defined above
   */
  private static Instant toInstant(TemporalAccessor temporalAccessor, ZoneId defaultZoneId) {
    // If the temporal accessor is a LocalDateTime, convert it to a ZonedDateTime using the provided
    // defaultTimeZone and proceed to the next step.
    if (temporalAccessor instanceof LocalDateTime) {
      temporalAccessor = ((LocalDateTime) temporalAccessor).atZone(defaultZoneId);
    }

    // If the temporal accessor is a ZonedDateTime, convert it to an Instant and return.  If it is
    // an Instant already, then simply return.  Otherwise, throw an exception.
    if (temporalAccessor instanceof Instant) {
      return (Instant) temporalAccessor;
    } else if (temporalAccessor instanceof ZonedDateTime) {
      return ((ZonedDateTime) temporalAccessor).toInstant();
    } else if (temporalAccessor instanceof OffsetDateTime) {
      return ((OffsetDateTime) temporalAccessor).toInstant();
    } else {
      throw new DateTimeException(
          temporalAccessor.getClass().getSimpleName() + " type is not supported");
    }
  }

  /**
   * Constructs a new instance; since this is a utility class, this constructor will never be used
   * and is provided only to suppress the default constructor.
   */
  private FlexTime() {
    // Do nothing.
  }
}
