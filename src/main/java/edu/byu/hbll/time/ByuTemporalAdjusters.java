package edu.byu.hbll.time;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;

/**
 * Utility class which exposes methods for generating {@link TemporalAdjuster} expressions unique to
 * Brigham Young University (BYU).
 *
 * <p>This class is patterned after and complements the {@link TemporalAdjusters} class in the
 * standard library. All methods in this class are thread-safe, "pure" functions, as are the {@link
 * TemporalAdjuster}s that they return.
 */
public class ByuTemporalAdjusters {

  /**
   * Returns the "plus working days" adjuster, which returns a new date set for the specified number
   * of working days (defined as all days that are not Saturdays, Sundays, or BYU University
   * Holidays) in the future.
   *
   * @param workingDays the number of working days to be added to a Temporal object
   * @return the temporal adjuster as described
   */
  public static TemporalAdjuster plusWorkingDays(int workingDays) {
    return temporal ->
        temporal.plus(toCalendarDays(toLocalDate(temporal), workingDays), ChronoUnit.DAYS);
  }

  /**
   * Returns the "plus working days" adjuster, which returns a new date set for the specified number
   * of working days (defined as all days that are not Saturdays, Sundays, or BYU University
   * Holidays) in the past.
   *
   * @param workingDays the number of working days to be added to a Temporal object
   * @return the temporal adjuster as described
   */
  public static TemporalAdjuster minusWorkingDays(int workingDays) {
    return plusWorkingDays(workingDays * -1);
  }

  /**
   * Returns a temporal adjuster which always evaluates to Instant.MAX or LocalDate.MAX, depending
   * on the type of Temporal provided.
   *
   * <p>This method is provided for convenience to make it safe to compare ISO-8601 calendar objects
   * as well as {@link Instant} objects using the same function.
   *
   * @return the temporal adjuster as described
   */
  public static TemporalAdjuster never() {
    return (temporal) ->
        temporal instanceof Instant ? temporal.with(Instant.MAX) : temporal.with(LocalDate.MAX);
  }

  /**
   * Returns the total number of calendar days corresponding to the given number of working days,
   * measured from the provided date.
   *
   * @param date the date from which to add or subtract days
   * @param workingDays the number of working days which should have occurred
   * @return the number of total calendar days corresponding with the passage/occurence of the given
   *     workingDays
   */
  private static int toCalendarDays(LocalDate date, int workingDays) {
    // If workingDays is positive, we simply add days and return a positive int.
    int scale = 1;
    // If workingDays is negative, we need to subtract days instead and return a negative int.
    if (workingDays < 0) {
      scale = -1;
      workingDays = workingDays * -1;
    }

    int workingDayCount = 0;
    int nonWorkingDayCount = 0;
    while (workingDayCount < workingDays) {
      date = date.plusDays(scale);
      if (ByuHolidays.isWorkingDay(date)) {
        workingDayCount++;
      } else {
        nonWorkingDayCount++;
      }
    }
    return (workingDayCount + nonWorkingDayCount) * scale;
  }

  /**
   * Converts an date/time to a {@link LocalDate}.
   *
   * <p>For ISO-8601 calendar-based temporal objects such as {@link ZonedDateTime} or {@link
   * LocalDateTime}, this conversion is straightforward. But notable {@link Instant}s are not
   * internally based on a calendar; rather they merely count the number of nanoseconds since {@code
   * 1970-01-01T00:00:00Z}. We must first convert the instant to its equivalent {@link
   * ZonedDateTime} before proceeding to reduce it to a {@link LocalDate}.
   *
   * @param temporal the temporal to convert to a local date
   * @return the local date corresponding with the provided temporal object
   */
  private static LocalDate toLocalDate(Temporal temporal) {
    Temporal calendarBasedTemporal = temporal;
    if (temporal instanceof Instant) {
      calendarBasedTemporal = ((Instant) temporal).atZone(ZoneId.systemDefault());
    }
    return LocalDate.from(calendarBasedTemporal);
  }

  /**
   * Constructs a new instance; since this is a utility class, this constructor will never be used
   * and is provided only to suppress the default constructor.
   */
  private ByuTemporalAdjusters() {
    // Do nothing.
  }
}
