package edu.byu.hbll.time;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Utility class which exposes methods for calculating the dates of Brigham Young University (BYU)
 * university holidays.
 *
 * <p>This class has been tested for calendar years 2013-2020, but should work outside those years
 * so long as BYU remains consistent in its determination of holidays. While there is an internal
 * cache used to memoize the {@link #isWorkingDay(LocalDate)} and {@link #universityHolidays(int)}
 * methods, it is private and thread-safe. As a result, all methods in this class are thread-safe,
 * "pure" functions.
 */
public class ByuHolidays {

  private static final int CACHE_CAPACITY = 100;
  private static final Map<Integer, List<LocalDate>> cache =
      Collections.synchronizedMap(
          new LinkedHashMap<Integer, List<LocalDate>>() {
            private static final long serialVersionUID = 1;

            @Override
            protected boolean removeEldestEntry(Map.Entry<Integer, List<LocalDate>> eldest) {
              return size() > CACHE_CAPACITY;
            }
          });

  /** The function used to compute holidays for any given year. */
  private static final Function<Integer, List<LocalDate>> computeHolidays =
      year -> {
        List<LocalDate> holidays = new ArrayList<>();

        holidays.addAll(beginningOfYear(year));
        holidays.add(martinLutherKingJrDay(year));
        holidays.add(presidentsDay(year));
        holidays.add(memorialDay(year));
        holidays.add(independenceDay(year));
        holidays.add(pioneerDay(year));
        holidays.add(laborDay(year));
        holidays.addAll(thanksgiving(year));
        holidays.addAll(christmas(year));
        holidays.addAll(endOfYear(year));

        return Collections.unmodifiableList(holidays);
      };

  /**
   * Determines whether the current date is a working or business day or not at BYU, based on
   * whether the date falls on a weekend or is a university holiday.
   *
   * @param date the date to be evaluated
   * @return {@code true} if the date is a working day, {@code false} otherwise
   */
  public static boolean isWorkingDay(LocalDate date) {
    DayOfWeek dayOfWeek = date.getDayOfWeek();
    return !dayOfWeek.equals(DayOfWeek.SATURDAY)
        && !dayOfWeek.equals(DayOfWeek.SUNDAY)
        && !universityHolidays(date.getYear()).contains(date);
  }

  /**
   * Returns the list of all BYU University Holidays during the given ISO year.
   *
   * <p>This method uses a Map cache behind the scenes to optimize repeated calls for a given year.
   *
   * @param year the year
   * @return the list of holidays for the year
   */
  public static List<LocalDate> universityHolidays(int year) {
    return cache.computeIfAbsent(year, computeHolidays);
  }

  /**
   * Returns the (January) university holiday(s) associated with New Year's.
   *
   * <p>Traditionally, BYU designates New Year's Eve and New Year's Day as university holidays.
   * However, when those days occur in proximity to a weekend, the university shifts the actual
   * dates of the university holidays to accommodate a 4-day weekend.
   *
   * <p>Since New Years, by definition, is celebrated at the boundary of two years, this means that
   * there may be 0, 1, or 2 holidays actually occuring during the new (incoming) year. The related
   * endOfYear method calculates the corresponding December holidays for the old (outgoing) year.
   *
   * @param year the year
   * @return the list of holiday dates
   */
  public static List<LocalDate> beginningOfYear(int year) {
    return fourDayWeekend(LocalDate.of(year, Month.JANUARY, 1)).stream()
        .filter(date -> date.getYear() == year)
        .collect(Collectors.toList());
  }

  /**
   * Returns the university holiday associated with Martin Luther King, Jr. Day.
   *
   * @param year the year
   * @return the holiday date
   */
  public static LocalDate martinLutherKingJrDay(int year) {
    return LocalDate.of(year, Month.JANUARY, 1)
        .with(TemporalAdjusters.dayOfWeekInMonth(3, DayOfWeek.MONDAY));
  }

  /**
   * Returns the university holiday associated with Presidents' Day.
   *
   * @param year the year
   * @return the holiday date
   */
  public static LocalDate presidentsDay(int year) {
    return LocalDate.of(year, Month.FEBRUARY, 1)
        .with(TemporalAdjusters.dayOfWeekInMonth(3, DayOfWeek.MONDAY));
  }

  /**
   * Returns the university holiday associated with Memorial Day.
   *
   * @param year the year
   * @return the holiday date
   */
  public static LocalDate memorialDay(int year) {
    return LocalDate.of(year, Month.MAY, 1).with(TemporalAdjusters.lastInMonth(DayOfWeek.MONDAY));
  }

  /**
   * Returns the university holiday associated with Independence Day.
   *
   * @param year the year
   * @return the holiday date
   */
  public static LocalDate independenceDay(int year) {
    return threeDayWeekend(LocalDate.of(year, Month.JULY, 4));
  }

  /**
   * Returns the university holiday associated with Pioneer Day.
   *
   * @param year the year
   * @return the holiday date
   */
  public static LocalDate pioneerDay(int year) {
    return threeDayWeekend(LocalDate.of(year, Month.JULY, 24));
  }

  /**
   * Returns the university holiday associated with Labor Day.
   *
   * @param year the year
   * @return the holiday date
   */
  public static LocalDate laborDay(int year) {
    return LocalDate.of(year, Month.SEPTEMBER, 1)
        .with(TemporalAdjusters.dayOfWeekInMonth(1, DayOfWeek.MONDAY));
  }

  /**
   * Returns the university holidays associated with Thanksgiving.
   *
   * <p>Since BYU has designated both Thanksgiving Day itself as well as the day after Thanksgiving
   * as university holidays, both dates will always be returned.
   *
   * @param year the year
   * @return the holiday date
   */
  public static List<LocalDate> thanksgiving(int year) {
    LocalDate holiday =
        LocalDate.of(year, Month.NOVEMBER, 1)
            .with(TemporalAdjusters.dayOfWeekInMonth(4, DayOfWeek.THURSDAY));
    return Arrays.asList(holiday, holiday.plusDays(1L));
  }

  /**
   * Returns the university holidays associated with Christmas.
   *
   * <p>Traditionally, BYU designates Christmas Eve and Christmas Day as university holidays.
   * However, when those dates occur in proximity to a weekend, the university shifts the actual
   * dates of the university holidays to accommodate a 4-day weekend.
   *
   * @param year the year
   * @return the holiday date
   */
  public static List<LocalDate> christmas(int year) {
    return fourDayWeekend(LocalDate.of(year, Month.DECEMBER, 25));
  }

  /**
   * Returns the (December) university holiday(s) associated with New Year's.
   *
   * <p>Traditionally, BYU designates New Year's Eve and New Year's Day as university holidays.
   * However, when those days occur in proximity to a weekend, BYU shifts the actual dates of the
   * university holidays to accommodate a 4-day weekend.
   *
   * <p>Since New Years, by definition, is celebrated at the boundary of two years, this means that
   * there may be 0, 1, or 2 holidays actually occuring during the old (outgoing) year. The related
   * beginningOfYear method calculates the corresponding January holidays for the new (incoming)
   * year.
   *
   * @param year the year
   * @return the list of holiday dates
   */
  public static List<LocalDate> endOfYear(int year) {
    return fourDayWeekend(LocalDate.of(year + 1, Month.JANUARY, 1)).parallelStream()
        .filter(date -> date.getYear() == year)
        .collect(Collectors.toList());
  }

  /**
   * For single-day holidays fixed to a date (such as Pioneer Day), this method will shift the date
   * of the corresponding university holiday to accommodate a 3-day weekend if possible.
   *
   * <p>The following logic is used to make the adjustment:
   *
   * <table>
   *   <tr>
   *     <th>Actual holiday occurs on:</th>
   *     <th>University holiday occurs on:</th>
   *   </tr>
   *   <tr>
   *     <td>Saturday</td>
   *     <td>Day before (Friday)</td>
   *   </tr>
   *   <tr>
   *     <td>Sunday</td>
   *     <td>Day after (Monday)</td>
   *   </tr>
   *   <tr>
   *     <td>Monday-Friday</td>
   *     <td>Same day (no adjustment)</td>
   *   </tr>
   * </table>
   *
   * @param primaryHoliday the date of the actual holiday
   * @return the adjusted date of the university holiday
   */
  private static LocalDate threeDayWeekend(LocalDate primaryHoliday) {
    switch (primaryHoliday.getDayOfWeek()) {
      case SATURDAY:
        return primaryHoliday.minusDays(1L);
      case SUNDAY:
        return primaryHoliday.plusDays(1L);
      default:
        return primaryHoliday;
    }
  }

  /**
   * For multi-day holidays fixed to a date (such as Christmas and New Year's), this method will
   * shift the dates of the corresponding university holiday to accommodate a 4-day weekend if
   * possible.
   *
   * <p>The following logic is used to make the adjustment, based on the date of the primary holiday
   * (e.g.: Christmas Day proper), which is assumed to be the last day of the traditional
   * celebration:
   *
   * <table>
   *   <tr>
   *     <th>Primary holiday occurs on:</th>
   *     <th>University holiday(s) occur on:</th>
   *   </tr>
   *   <tr>
   *     <td>Thursday</td>
   *     <td>The same day and the day after (Thursday and Friday)</td>
   *   </tr>
   *   <tr>
   *     <td>Saturday</td>
   *     <td>The two days before (Thursday and Friday)</td>
   *   </tr>
   *   <tr>
   *     <td>Sunday</td>
   *     <td>Two days before and one day after (Friday and Monday)</td>
   *   </tr>
   *   <tr>
   *     <td>Monday</td>
   *     <td>The same day and the day after (Monday and Tuesday)</td>
   *   </tr>
   *   <tr>
   *     <td>Tuesday, Wednesday, Friday</td>
   *     <td>The day before and the same day (no adjustment)</td>
   *   </tr>
   *
   * </table>
   *
   * @param primaryHoliday the date of the actual holiday
   * @return the adjusted date of the university holiday
   */
  private static List<LocalDate> fourDayWeekend(LocalDate primaryHoliday) {
    switch (primaryHoliday.getDayOfWeek()) {
      case SATURDAY:
        return Arrays.asList(primaryHoliday.minusDays(2L), primaryHoliday.minusDays(1L));
      case SUNDAY:
        return Arrays.asList(primaryHoliday.minusDays(2L), primaryHoliday.plusDays(1L));
      case THURSDAY:
      case MONDAY:
        return Arrays.asList(primaryHoliday, primaryHoliday.plusDays(1L));
      default:
        return Arrays.asList(primaryHoliday.minusDays(1L), primaryHoliday);
    }
  }

  /**
   * Constructs a new instance; since this is a utility class, this constructor will never be used
   * and is provided only to suppress the default constructor.
   */
  private ByuHolidays() {
    // Do nothing.
  }
}
